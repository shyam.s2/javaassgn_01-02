package or.Assg;

public class Area extends Shape {

	@Override
	void RectangleArea(double l, double b) {
		System.out.println("Area of Rectangle: " + (l * b));
		
	}

	@Override
	void SquareArea(double s) {
		
		System.out.println("Area of Square: " + (s * s));
			
	}

	@Override
	void CircleArea(double r) {
		System.out.println("Area of Circle: " + (3.14 * r * r));
		
		
	}
	public static void main(String[] args) {
	    Area obj = new Area();
	    obj.RectangleArea(6, 22);
	    obj.SquareArea(6);
	    obj.CircleArea(6);
	  }
}
