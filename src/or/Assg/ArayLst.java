package or.Assg;

import java.util.ArrayList;

public class ArayLst {
  public static void main(String[] args) {
    ArrayList<String> products = new ArrayList<>();
    products.add("Coke");
    products.add("Pepsi");
    products.add("Sprite");
    products.add("Fanta");
    products.add("ThumbsUp");
    
    System.out.println("Size of the list: " + products.size());
    System.out.println("Contents of the list: " + products);
    
    products.remove(1);
    System.out.println("After removing the 2nd item, size of the list: " + products.size());
    System.out.println("Contents of the list: " + products);
    
    boolean isPresent = products.contains("Coke");
    System.out.println("Is Coke present in the list? " + isPresent);
  }
}